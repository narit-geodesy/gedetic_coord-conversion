# Base on Textbook :ISBN 978-0-13-375888-7
# Takes a geodetic coordinate (latitude, longitude, ellipsoid height) and 
# returns a geocentric coordinate with its corresponding X, Y and Z.
# Default Ellipsoid is GRS80 parameters.

from math import (radians, sqrt, sin, cos)

# Decimal Angle
latitude  = 18.864323475271
longitude = 99.216958897619

# DMS Angle
#latitude  = '+ 18 51 51.5645'
#longitude = '+ 99 13 01.0520'

height = 372.22310439

# Angles Module
def dms2dec(angle):
    angle = angle.split()
    angle_dec = float(angle[1]) + float(angle[2])/60 + float(angle[3])/3600
    
    if angle[0] == '-':
        return float(angle_dec *(-1))
    else:
        return float(angle_dec)

def angular_typecheck(angle):
    # Converts Angle Objects to Decimal Degrees (float) for computations
    # :Support format: 18.86472592 type=<float>, '+ 18 51 53.01330' type=<str>
    
    supported_types = ["<class 'str'>"]
    if str(type(angle)) in supported_types:
        return float(dms2dec(angle))
    else:
        return float(angle)

# Constants Module
# Ellipsoid constants
class Ellipsoid(object):
    def __init__(self, semimaj, inversef, type):
        self.semimaj = semimaj
        self.inversef = inversef
        self.f = 1 / self.inversef
        self.semimin = float(self.semimaj * (1 - self.f))
        self.ecc1sq = float(self.f * (2 - self.f))
        self.ecc2sq = float(self.ecc1sq / (1 - self.ecc1sq))
        self.ecc1 = sqrt(self.ecc1sq)
        self.n = float(self.f / (2 - self.f))
        self.n2 = self.n ** 2
        self.meanradius = (2 * self.semimaj + self.semimin)/3
        self.type = type

# Geodetic Reference System 1980
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7019
grs80 = Ellipsoid(6378137, 298.257222101, 'grs80')

# World Geodetic System 1984
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7030
wgs84 = Ellipsoid(6378137, 298.257223563, 'wgs84')

# Australian National Spheroid
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7003
ans = Ellipsoid(6378160, 298.25, 'ans')

# International (Hayford) 1924
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7022
intl24 = Ellipsoid(6378388, 297, 'intl24')

def geo2xyz(lat, lon, hei, ellipsoid):
    # :param lat: Latitude
    # :type lat: Float (Decimal Degrees)
    # :param lon: Longitude
    # :type lon: Float (Decimal Degrees)
    # :param hei: Ellipsoidal height (meters)
    # :param ellipsoid: Ellipsoid Object
    # :type ellipsoid: Ellipsoid
    
    # :return: X (m), Y (m), Z (m)
    # :rtype: tuple
    
    # Debug
    print('TYPE     {}'.format(ellipsoid.type))
    print('SEMIMAJ  {}'.format(ellipsoid.semimaj))
    print('INVERSEF {}'.format(ellipsoid.inversef))
    print('ECC1     {}'.format(ellipsoid.ecc1))

    lat = radians(angular_typecheck(lat))
    lon = radians(angular_typecheck(lon))
    Rn  = ellipsoid.semimaj / (sqrt( 1 - ellipsoid.ecc1sq * (sin(lat))**2))

    # Geocentric coordinate
    X = (Rn + hei) * cos(lat) * cos(lon)
    Y = (Rn + hei) * cos(lat) * sin(lon)
    Z = (Rn * (1 - ellipsoid.ecc1sq) + hei) * sin(lat)
    
    return X, Y, Z

# Defines Ellipsoid parameters
# :support Ellipsoid :grs80, wgs84, ans, intl24
ellipsoid = grs80

X, Y, Z = geo2xyz(latitude, longitude, height, ellipsoid)

print('X        {:.4f}'.format(X))
print('Y        {:.4f}'.format(Y))
print('Z        {:.4f}'.format(Z))