# Base on Textbook :ISBN 978-0-13-375888-7
# Takes a geocentric coordinate (X, Y, Z) and returns a geodetic
# coordinate with its corresponding latitude, longitude, and
# ellipsoid height. Default Ellipsoid is GRS80 parameters.

from math import (pi, sqrt, atan, sin, cos)

X = -967128.775  
Y = 5960055.199
Z = 2049264.045

# Constants Module
# Ellipsoid constants
class Ellipsoid(object):
    def __init__(self, semimaj, inversef, type):
        self.semimaj = semimaj
        self.inversef = inversef
        self.f = 1 / self.inversef
        self.semimin = float(self.semimaj * (1 - self.f))
        self.ecc1sq = float(self.f * (2 - self.f))
        self.ecc2sq = float(self.ecc1sq / (1 - self.ecc1sq))
        self.ecc1 = sqrt(self.ecc1sq)
        self.n = float(self.f / (2 - self.f))
        self.n2 = self.n ** 2
        self.meanradius = (2 * self.semimaj + self.semimin)/3
        self.type = type

# Geodetic Reference System 1980
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7019
grs80 = Ellipsoid(6378137, 298.257222101, 'grs80')

# World Geodetic System 1984
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7030
wgs84 = Ellipsoid(6378137, 298.257223563, 'wgs84')

# Australian National Spheroid
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7003
ans = Ellipsoid(6378160, 298.25, 'ans')

# International (Hayford) 1924
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7022
intl24 = Ellipsoid(6378388, 297, 'intl24')

def xyz2geo(X, Y, Z, ellipsoid):
    # :param X: X
    # :type X: Float (m)
    # :param Y: Y
    # :type Y: Float (m)
    # :param Z: Z
    # :type Z: Float (m)
    # :param ellipsoid: Ellipsoid Object
    # :type ellipsoid: Ellipsoid
    
    # :return: latitude (Decimal Degree), longitude (Decimal Degree), ellipsoidal height (m)
    # :rtype: tuple

    a = ellipsoid.semimaj
    f = ellipsoid.inversef
    e = ellipsoid.ecc1
    
    # Debug
    print('TYPE     {}'.format(ellipsoid.type))
    print('SEMIMAJ  {}'.format(a))
    print('INVERSEF {}'.format(f))
    print('ECC1     {}'.format(e))

    # Displacement on equatorial plane (D)
    D = sqrt(X**2 + Y**2)

    # Longitude
    lon = 2 * atan((D - X) / Y)  
    
    # Lattitude
    for i in range (0, 1000):
        if i == 0:
            lat_approximate = atan(Z / (D * ( 1-e**2 )))
            Rn_approximate  = a / (sqrt( 1 - (e * sin(lat_approximate))**2 ))
            
            # The 1st Lattitude
            lat = atan((Z + ( e**2 * Rn_approximate * sin(lat_approximate) )) / D)
            #print('DEL:{:.20f}, Rn:{:.8f}'.format(abs(lat_approximate - lat), Rn_approximate))

        else:
            #lat_for_debug = lat
            Rn  = a / (sqrt( 1 - (e * sin(lat))**2 ))
            lat = atan((Z + ( e**2 * Rn * sin(lat) )) / D)
            #print('DEL:{:.20f}, Rn:{:.8f}'.format(abs(lat_for_debug - lat), Rn))
    
    # Height
    if abs(lat) <= 45:
        hei = (D / cos(lat)) - Rn
    else:
        hei = (Z / sin(lat)) - Rn * (1 - e**2)
    
    # convert unit of [rad] to [deg]
    lat = lat * 180/pi
    lon = lon * 180/pi

    return lat, lon, hei

# Defines Ellipsoid parameters
# :support Ellipsoid :grs80, wgs84, ans, intl24
ellipsoid = grs80

# Result of geodetic coordinate
LAT, LON, HEI = xyz2geo(X, Y, Z, ellipsoid)

print('LAT      {:.12f}'.format(LAT))
print('LON      {:.12f}'.format(LON))
print('HEI      {:.8f}'.format(HEI))