# Base on Geoscience Australia - Python Geodesy Package
# Takes a Transverse Mercator grid co-ordinate (Zone, Easting, Northing,
# Hemisphere) and returns its corresponding Geographic Latitude and Longitude,
# Point Scale Factor and Grid Convergence. Default Projection is Universal
# Transverse Mercator Projection with GRS80 Ellipsoid parameters

import warnings
from math import (sin, cos, atan2, radians, degrees, sqrt, cosh, sinh, tan, atan, log)

# Grid value
Zone  = 47
Easting  = 522853.388
Northing = 2085829.244

# Angles Module
def dms2dec(angle):
    angle = angle.split()
    angle_dec = float(angle[1]) + float(angle[2])/60 + float(angle[3])/3600
    
    if angle[0] == '-':
        return float(angle_dec *(-1))
    else:
        return float(angle_dec)

def angular_typecheck(angle):
    # Converts Angle Objects to Decimal Degrees (float) for computations
    # :Support format: 18.86472592 type=<float>, '+ 18 51 53.01330' type=<str>
    
    supported_types = ["<class 'str'>"]
    if str(type(angle)) in supported_types:
        return float(dms2dec(angle))
    else:
        return float(angle)

# Constants Module
# Ellipsoid constants
class Ellipsoid(object):
    def __init__(self, semimaj, inversef, type):
        self.semimaj = semimaj
        self.inversef = inversef
        self.f = 1 / self.inversef
        self.semimin = float(self.semimaj * (1 - self.f))
        self.ecc1sq = float(self.f * (2 - self.f))
        self.ecc2sq = float(self.ecc1sq / (1 - self.ecc1sq))
        self.ecc1 = sqrt(self.ecc1sq)
        self.n = float(self.f / (2 - self.f))
        self.n2 = self.n ** 2
        self.meanradius = (2 * self.semimaj + self.semimin)/3
        self.type = type

# Geodetic Reference System 1980
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7019
grs80 = Ellipsoid(6378137, 298.257222101, 'grs80')

# World Geodetic System 1984
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7030
wgs84 = Ellipsoid(6378137, 298.257223563, 'wgs84')

# Australian National Spheroid
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7003
ans = Ellipsoid(6378160, 298.25, 'ans')

# International (Hayford) 1924
# www.epsg-registry.org/export.htm?gml=urn:ogc:def:ellipsoid:EPSG::7022
intl24 = Ellipsoid(6378388, 297, 'intl24')

# Projections
class Projection(object):
    def __init__(self, falseeast, falsenorth, cmscale, zonewidth, initialcm):
        # :param falseeast: Easting (m) assigned to Central Meridian
        # :param falsenorth: Northing (m) assigned to Equator
        # :param cmscale: Central Meridian Scale Factor (unitless, 1 is no scale)
        # :param zonewidth: Width (decimal degrees) of each TM Zone
        # :param initialcm: Longitude (decimal degrees) of TM Zone 1

        self.falseeast = falseeast
        self.falsenorth = falsenorth
        self.cmscale = cmscale
        self.zonewidth = zonewidth
        self.initialcm = initialcm

utm = Projection(500000, 10000000, 0.9996, 6, -177)

# Integrated Survey Grid - used in NSW as the projection for AGD66
# Spatial Services projections page - https://www.spatial.nsw.gov.au/surveying/geodesy/projections
# ISG Technical Manual - https://www.spatial.nsw.gov.au/__data/assets/pdf_file/0017/25730/ISG.pdf
isg = Projection(300000, 5000000, 0.99994, 2, -177)

def rect_radius(ellipsoid):
    # Computes the Rectifying Radius of an Ellipsoid with specified Inverse
    # Flattening (See Ref 2 Equation 3)
    # :param ellipsoid: Ellipsoid Object
    # :type ellipsoid: Ellipsoid
    # :return: Ellipsoid Rectifying Radius
    nval = (1 / float(ellipsoid.inversef)) /\
           (2 - (1 / float(ellipsoid.inversef)))
    nval2 = nval**2
    return (ellipsoid.semimaj / (1 + nval) * ((nval2 *
                                              (nval2 *
                                               (nval2 *
                                                (25 * nval2 + 64)
                                                + 256)
                                               + 4096)
                                              + 16384)
                                              / 16384.))

def alpha_coeff(ellipsoid):
    # Computes the set of Alpha coefficients of an Ellipsoid with specified
    # Inverse Flattening (See Ref 2 Equation 5)
    # :param ellipsoid: Ellipsoid Object
    # :type ellipsoid: Ellipsoid
    # :return: Alpha coefficients a2, a4 ... a16
    # :rtype: tuple
    nval = ellipsoid.n
    a2 = ((nval *
           (nval *
            (nval *
             (nval *
              (nval *
               (nval *
                ((37884525 - 75900428 * nval)
                 * nval + 42422016)
                - 89611200)
               + 46287360)
              + 63504000)
             - 135475200)
            + 101606400))
          / 203212800.)

    a4 = ((nval ** 2 *
           (nval *
            (nval *
             (nval *
              (nval *
               (nval *
                (148003883 * nval + 83274912)
                - 178508970)
               + 77690880)
              + 67374720)
             - 104509440)
            + 47174400))
          / 174182400.)

    a6 = ((nval ** 3 *
           (nval *
            (nval *
             (nval *
              (nval *
               (318729724 * nval - 738126169)
               + 294981280)
              + 178924680)
             - 234938880)
            + 81164160))
          / 319334400.)

    a8 = ((nval ** 4 *
           (nval *
            (nval *
             ((14967552000 - 40176129013 * nval) * nval + 6971354016)
             - 8165836800)
            + 2355138720))
          / 7664025600.)

    a10 = ((nval ** 5 *
            (nval *
             (nval *
              (10421654396 * nval + 3997835751)
              - 4266773472)
             + 1072709352))
           / 2490808320.)

    a12 = ((nval ** 6 *
            (nval *
             (175214326799 * nval - 171950693600)
             + 38652967262))
           / 58118860800.)

    a14 = ((nval ** 7 *
            (13700311101 - 67039739596 * nval))
           / 12454041600.)

    a16 = (1424729850961 * nval ** 8) / 743921418240.
    return a2, a4, a6, a8, a10, a12, a14, a16

def beta_coeff(ellipsoid):
    # Computes the set of Beta coefficients of an Ellipsoid with specified
    # Inverse Flattening (See Ref 2 Equation 23)
    # :param ellipsoid: Ellipsoid Object
    # :type ellipsoid: Ellipsoid
    # :return: Alpha coefficients a2, a4 ... a16
    # :rtype: tuple
    nval = ellipsoid.n
    b2 = ((nval *
           (nval *
            (nval *
             (nval *
              (nval *
               (nval *
                ((37845269 - 31777436 * nval) - 43097152)
                + 42865200)
               + 752640)
              - 104428800)
             + 180633600)
            - 135475200))
          / 270950400.)

    b4 = ((nval ** 2 *
           (nval *
            (nval *
             (nval *
              (nval *
               ((-24749483 * nval - 14930208) * nval + 100683990)
               - 152616960)
              + 105719040)
             - 23224320)
            - 7257600))
          / 348364800.)

    b6 = ((nval ** 3 *
           (nval *
            (nval *
             (nval *
              (nval *
               (232468668 * nval - 101880889)
               - 39205760)
              + 29795040)
             + 28131840)
            - 22619520))
          / 638668800.)

    b8 = ((nval ** 4 *
           (nval *
            (nval *
             ((-324154477 * nval - 1433121792) * nval + 876745056)
             + 167270400)
            - 208945440))
          / 7664025600.)

    b10 = ((nval ** 5 *
            (nval *
             (nval *
              (312227409 - 457888660 * nval)
              + 67920528)
             - 70779852))
           / 2490808320.)

    b12 = ((nval ** 6 *
            (nval *
             (19841813847 * nval + 3665348512)
             - 3758062126))
           / 116237721600.)

    b14 = ((nval ** 7 *
            (1989295244 * nval - 1979471673))
           / 49816166400.)

    b16 = ((-191773887257 * nval ** 8) / 3719607091200.)
    return b2, b4, b6, b8, b10, b12, b14, b16

def psfandgridconv(xi1, eta1, lat, lon, cm, conf_lat, ellipsoid, prj):
    # Calculates Point Scale Factor and Grid Convergence. Used in convert.geo2grid
    # and convert.grid2geo
    # :param xi1: Transverse Mercator Ratio Xi
    # :param eta1: Transverse Mercator Ratio Eta
    # :param lat: Latitude
    # :type lat: Decimal Degrees, DMSAngle or DDMAngle
    # :param lon: Longitude
    # :type lon: Decimal Degrees, DMSAngle or DDMAngle
    # :param cm: Central Meridian
    # :param conf_lat: Conformal Latitude
    # :param ellipsoid: Ellipsoid Object (default: GRS80)
    # :return: Point Scale Factor, Grid Convergence (Decimal Degrees)
    # :rtype: tuple
    lat = angular_typecheck(lat)
    lon = angular_typecheck(lon)
    A = rect_radius(ellipsoid)
    a = alpha_coeff(ellipsoid)
    lat = radians(lat)
    long_diff = radians(lon - cm)

    # Point Scale Factor
    p = 1
    q = 0
    for r in range(1, 9):
        p += 2*r * a[r-1] * cos(2*r * xi1) * cosh(2*r * eta1)
        q += 2*r * a[r-1] * sin(2*r * xi1) * sinh(2*r * eta1)
    q = -q
    psf = (float(prj.cmscale)
           * (A / ellipsoid.semimaj)
           * sqrt(q**2 + p**2)
           * ((sqrt(1 + (tan(lat)**2))
               * sqrt(1 - ellipsoid.ecc1sq * (sin(lat)**2)))
              / sqrt((tan(conf_lat)**2) + (cos(long_diff)**2))))

    # Grid Convergence
    grid_conv = degrees(atan(abs(q / p))
                        + atan(abs(tan(conf_lat) * tan(long_diff))
                               / sqrt(1 + tan(conf_lat)**2)))
    if cm > lon and lat < 0:
        grid_conv = -grid_conv
    elif cm < lon and lat > 0:
        grid_conv = -grid_conv

    return psf, grid_conv

def grid2geo(zone, east, north, hemisphere, ellipsoid, prj):
    # :param zone: Zone Number - 1 to 60
    # :param east: Easting (m, within 3330km of Central Meridian)
    # :param north: Northing (m, 0 to 10,000,000m)
    # :param hemisphere: String - 'north' or 'south'
    # :param ellipsoid: Ellipsoid Object
    # :type ellipsoid: Ellipsoid
    
    # :return: Latitude and Longitude (Decimal Degrees), Point Scale Factor,
    #          Grid Convergence (Decimal Degrees)
    # :rtype: tuple
    
    # Debug
    print('TYPE         {}'.format(ellipsoid.type))
    print('SEMIMAJ      {}'.format(ellipsoid.semimaj))
    print('INVERSEF     {}'.format(ellipsoid.inversef))
    print('CM SCALE     {}'.format(prj.cmscale))

    # Input Validation - UTM Extents and Values
    zone = int(zone)
    if prj == isg:
        if zone not in (541, 542, 543, 551, 552, 553, 561, 562, 563, 572):
            raise ValueError('Invalid Zone - Choose from 541, 542, 543, 551, 552, 553, 561, 562, 563, 572')
    else:
        if zone < 0 or zone > 60:
            raise ValueError('Invalid Zone - Zones from 1 to 60')

    if east < -2830000 or east > 3830000:
        raise ValueError('Invalid Easting - Must be within'
                         '3330km of Central Meridian')

    if north < 0 or north > 10000000:
        raise ValueError('Invalid Northing - Must be between 0 and 10,000,000m')

    h = hemisphere.lower()
    if h != 'north' and h != 'south':
        raise ValueError('Invalid Hemisphere - String, either North or South')

    if prj == isg and ellipsoid != ans:
        warnings.warn(message='ISG projection should be used with ANS ellipsoid', category=UserWarning)

    A = rect_radius(ellipsoid)
    b = beta_coeff(ellipsoid)
    # Transverse Mercator Co-ordinates
    x = (east - float(prj.falseeast)) / float(prj.cmscale)
    if hemisphere.lower() == 'north':
        y = -(north / float(prj.cmscale))
        hemisign = -1
    else:
        y = (north - float(prj.falsenorth)) / float(prj.cmscale)
        hemisign = 1

    # Transverse Mercator Ratios
    xi = y / A
    eta = x / A

    # Gauss-Schreiber Ratios
    eta1 = eta
    xi1 = xi
    for r in range(1, 9):
        eta1 += b[r-1] * cos(2*r * xi) * sinh(2*r * eta)
        xi1 += b[r-1] * sin(2*r * xi) * cosh(2*r * eta)

    # Conformal Latitude
    conf_lat = (sin(xi1)) / (sqrt((sinh(eta1)) ** 2 + (cos(xi1)) ** 2))
    t1 = conf_lat
    conf_lat = atan(conf_lat)

    # Finding t using Newtons Method
    def sigma(tn, ecc1):
        return (sinh(ecc1
                     * 0.5
                     * log((1 + ((ecc1 * tn) / (sqrt(1 + tn ** 2))))
                           / (1 - ((ecc1 * tn) / (sqrt(1 + tn ** 2)))))))

    def ftn(tn, ecc1):
        return (t * sqrt(1 + (sigma(tn, ecc1)) ** 2) -
                sigma(tn, ecc1) * sqrt(1 + tn ** 2) - t1)

    def f1tn(tn, ecc1, ecc1sq):
        return ((sqrt(1 + (sigma(tn, ecc1)) ** 2) * sqrt(1 + tn ** 2)
                 - sigma(tn, ecc1) * tn)
                * (((1 - float(ecc1sq)) * sqrt(1 + t ** 2))
                   / (1 + (1 - float(ecc1sq)) * t ** 2)))

    diff = 1
    t = t1
    itercount = 0
    while diff > 1e-15 and itercount < 100:
        itercount += 1
        t_before = t
        t = t - (ftn(t, ellipsoid.ecc1)
                 / f1tn(t, ellipsoid.ecc1, ellipsoid.ecc1sq))
        diff = abs(t - t_before)
    lat = degrees(atan(t))

    # Compute Longitude
    if prj == isg:
        amgzone = int(str(zone)[:2])
        subzone = int(str(zone)[2])
        cm = float((amgzone - 1) * prj.zonewidth * 3 + prj.initialcm + (subzone - 2) * prj.zonewidth)
    else:
        cm = float((zone * prj.zonewidth) + prj.initialcm - prj.zonewidth)
    long_diff = degrees(atan(sinh(eta1) / cos(xi1)))
    long = cm + long_diff

    # Point Scale Factor and Grid Convergence
    psf, grid_conv = psfandgridconv(xi1, eta1, lat, long, cm, conf_lat, ellipsoid, prj)

    return (hemisign * lat, long, psf, hemisign * grid_conv)

# Defines Ellipsoid parameters and Projection type
# :support hemisphere :'north', 'south'
# :support ellipsoid  :grs80, wgs84, ans, intl24
# :support projection :utm, isg
Hemisphere = 'north'
ellipsoid = grs80
prj = utm

LAT, LON, PSF, GRID_CONV = grid2geo(Zone, Easting, Northing, Hemisphere, ellipsoid, prj)

print('LATITUDE     {:.8f}'.format(LAT))
print('LONGITUDE    {:.8f}'.format(LON))
print('PSF          {:.8f}'.format(PSF))
print('GRID_CONV    {:.8f}'.format(GRID_CONV))